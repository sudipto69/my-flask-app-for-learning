#!/bin/bash

docker build -t node-app .

docker run -d -p 3000:3000 --name node-app node-app
